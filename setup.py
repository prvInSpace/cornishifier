from setuptools import setup, find_namespace_packages


packages = find_namespace_packages('src')

setup(
    name='cornishify',
    version='v23.08',
    description='Library used to transliterate Welsh into Cornish Standard Written Form',
    author='Preben Vangberg',
    author_email='prv21fgt@bangor.ac.uk',
    packages=packages,
    package_dir={'': 'src'},
    include_package_data=True,
    install_requires=[],
    extras_require={
        'dev': ['pytest']
    }
)