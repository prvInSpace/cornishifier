# Cornishifier

This is a simple package to convert Welsh orthography to Cornish orthography. The output is gibberish but it might aid Cornish ASR models fine-tuned on Welsh models.