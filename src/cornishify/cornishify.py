from dataclasses import dataclass
import re


@dataclass
class Replacement:
    welsh: str
    cornish_swf: str


REPLACEMENTS = [
    Replacement("ch", "gh"), # Has to be before c
    Replacement("c", "k"), 
    Replacement("dd", "dh"),
    Replacement("ff", "f"),  # Has to be before f
    Replacement("f", "v"),
]


def cornishify(text: str) -> str:
    regex = re.compile(f"({'|'.join([x.welsh for x in REPLACEMENTS])})")
    lookup = {x.welsh: x for x in REPLACEMENTS}

    # Needs to be reversed so that replacement is easier
    for match in reversed(list(regex.finditer(text))):
        # Do the whole replacing thing by replacing splicing away the match and
        # replacing it with the Cornish Standard Written Form
        text = (
            text[: match.start()]
            + lookup.get(match.group()).cornish_swf
            + text[match.end() :]
        )
    
    return text
