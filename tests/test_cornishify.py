
import pytest

from cornishify.cornishify import cornishify

@pytest.mark.parametrize("welsh,cornish_swf", [
    ("Senedd", "Senedh"),
    ("ff f", "f v"),
    ("f ff", "v f")
])
def test_transliteration(welsh, cornish_swf):
    assert cornish_swf == cornishify(welsh)
